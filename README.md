# Aide et ressources de Fiji - ImageJ pour Synchrotron SOLEIL

[<img src="https://imagej.net/_images/a/ae/Fiji-icon.png" width="250"/>](https://imagej.net/Fiji)

## Résumé

- Développer des scripts / workflows d'analyse d'images, de stacks, de réduction de données.
- Open source

## Sources

- Code source: https://github.com/fiji - https://imagej.nih.gov/ij/source/
- Documentation officielle: https://imagej.net/Welcome - https://imagej.nih.gov/ij/

## Navigation rapide

| Tutoriaux | Ressources annexes |
| ------ | ------ |
| [Tutoriel d'installation officiel](https://imagej.nih.gov/ij/docs/install/index.html) | [ImageJ dans le navigateur](https://ij.imjoy.io/) |
| [Tutoriaux officiels](https://imagej.net/Category:Tutorials) | [Guides d'utilisateurs](https://imagej.net/User_Guides) |
| [Chaine Youtube officielle](https://www.youtube.com/user/fijichannel/videos)  | |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS, Machine virtuelle
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: TIFF, collection d'images dans des sous dossiers, Bio-Format, hdf5, ...
- en sortie: Idem, le plus souvent collection d'images dans des sous dossiers.
- sur un disque dur
